#+TITLE: Stacks Practice (Linked lists) HTML
#+AUTHOR: VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Practice artefact for Stacks using Linked lists
** Features of the artefact
+ Artefact provides practice for stacks using linked lists.
+ User can see an empty array in the beginning.
+ User can click the =Push= button to push the entered
  element into stack.
+ User can click the =Pop= button to pop the top most
  element of the stack.
+ User can click the =clear= button to clear the stack.
+ Artefact provides step by step feedback to user
+ If user's step is wrong, error message with appropriate
  hint is displayed. Else, step is performed.

* HTML Framework of stacks practice Artefact using linked lists
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Stacks Practice</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/stacks-and-queues.css">
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
#+END_SRC

** Body Section Elements
*** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content"> 
    <ul>
      <li>Enter a value and click <b>push</b> to add the element into the linked list (stack).</li>
      <li>Click <b>pop</b> to remove the top element.</li>
      <li>Click <b>Clear</b> to clear the linked list (stack).</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Canvas for Stack(i.e linked list)
This is the Canvas used for linked list
#+NAME: canvas-linkedlist
#+BEGIN_SRC html
<canvas id="linkedlist" width="1360" height="260">
    Your browser does not support canvas.
</canvas>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box" id="comments-stdemo">
  <b>Observations</b><br>
  <p id="ins"></p>
</div>

#+END_SRC

*** Buttons Wrapper
This are the controller buttons(New Question, Reset, Submit)
of the artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input class="text-box" id="HeadtoBeInserted" type="alphanumeric" autocomplete="off" class="leftinput" placeholder="Enter an element" maxlength="3">
  <input type="button" class="button-input" value="Push" id="push-button-sll">
  <input type="button" class="button-input" value="pop" id="pop-button-sll">
  <input type="button" class="button-input" value="Clear" id="clear-button">
</div>

#+END_SRC

*** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems 
#+BEGIN_SRC html
<script src="../js/stack-linkedlist.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC 

* Tangle
#+BEGIN_SRC html :tangle stack-linkedlist.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <<instruction-box>>
    <<canvas-linkedlist>>
    <<comments-box>>
    <<buttons-wrapper>>
    <<js-elems>>
  </body>
</html>
#+END_SRC


