#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Demonstration artefact for Queues
** Features of the artefact
+ Artefact provides demo for Queue basic operations.
+ User can see an array(i.e Queue A) of a predefined word
  but the letters in the word will be jumbled.
+ At the end of demonstration the proper word will be
  displayed in Queue B and Queue A gets empty.
+ User can click the =Reset= button to generate the jumbled
  letter predefined word.
+ User shall click the =Start= button to start the demo.
+ User can slide a =Slider= to adjust speed of the demo as
  per his/her convinience.
+ User can pause the demo by clicking on the =Pause= button.
+ User can pause and then click on =Next= manually to view
  the demo at his/her own speed.
 
* HTML Framework of Queue Demo Artefact
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Queues Demonstration</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
<link rel="stylesheet" href="../css/stacks-and-queues.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Click on the <b>Start</b> button to start the
        demo.</li>
      <li>Move the <b>slider</b> to adjust the speed of the
        demo.</li>
      <li>Click on the <b>Pause</b> button if you want to
        stop and manually click the <b>Next</b> button to
        have a step by step realization of the process.</li>
      <li>Click on the <b>Reset</b> button to reset the
        queue into its initial position</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Question Holder
This will hold the question of Queue Demo artefact.
#+NAME: question-holder
#+BEGIN_SRC html
<p id="question-holder">
  <b>Observe:</b> The letters in the word "ENCRYPT" are
  stored in Queue A in a random manner. Observe the
  operations performed to display the letters in their
  correct order in Queue B so as to form the actual word.
</p>

#+END_SRC

*** Canvas for Queue A
This is the Canvas used for Queue A
#+NAME: canvas-queueA
#+BEGIN_SRC html
<canvas id="queue-demo-canvas1" width="1200" height="230">
    Your browser does not support canvas.
</canvas>

#+END_SRC

*** Canvas for Queue B
This is the Canvas used for Queue B
#+NAME: canvas-queueB
#+BEGIN_SRC html
<canvas id="queue-demo-canvas2" width="1200" height="230">
    Your browser does not support canvas.
</canvas>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box" id="comments-qdemo">
  <b>Observations</b><br>
  <p id="ins"></p>
</div>

#+END_SRC

*** Slider Container
This is a slider to which =change_interval= method is
appended to control the demonstration speed of an artefact.
#+NAME: slider-container
#+BEGIN_SRC html
<div class="slidecontainer" id="slide-queuedemo">
  <p>
    Min. Speed
    <input class="slider" type="range" min="100" max="2500" id="interval" value="1500" >
    Max. Speed
  </p>
</div>

#+END_SRC

*** Buttons Wrapper
This are the controller buttons(Start, Reset, Pause) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: buttons-wrapper
#+BEGIN_SRC html
<div class="buttons-wrapper">
  <input type="button" class="button-input" value="Start" id="startbutton-queuedemo" >
  <input type="button" class="button-input" value="Reset" id="reset-queuedemo">
  <input type="button" class="button-input" value="Pause" id="pause-queuedemo" >
</div>

#+END_SRC

*** Javascript
This comprises of all the Javascript needed to run or add
behavior to the artefact.
#+NAME: js-elems 
#+BEGIN_SRC html
<script src="../js/queue-demo.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC 

* Tangle
#+BEGIN_SRC html :tangle queue-demo.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body onload="initializeArtefact();">
    <<instruction-box>>
    <<question-holder>>
    <<canvas-queueA>>
    <<canvas-queueB>>
    <<comments-box>>
    <<slider-container>> 
    <<buttons-wrapper>>
    <<js-elems>>
  </body>
</html>
#+END_SRC


