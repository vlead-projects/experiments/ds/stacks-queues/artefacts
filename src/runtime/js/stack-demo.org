#+TITLE: Stacks Operations Demonstration
#+AUTHOR: VLEAD
#+DATE: [2018-06-12 Tue]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document contains the JavaScript code (Canvas API) for the
requirement artifact - stacks operations Demonstration.
 
The artefact provides stack A with a set of numbers.On clicking start ,the 
demonstration starts. Numbers from stack A are popped and if it is a prime
it is pushed into stack B else pushed into stack C.At the end of demonstration 
all the numbers of stack A gets popped.

* stack_class
+ Creates =stack_class= class, which has all the variables used for canvas attributes and its corresponding
positions on the canvas .
+ We use "let" to create an object of this class
+ Here we initialize the =canvas= and define a class name =stack_class=
+ Font of numbers to be inserted is defined by =txtSize=
+ Distance between lines is defined by =lineDist=
+ Number of array blocks is defined by =rectNo=
+ Number of lines to seperate the array blocks is defined by =lineNo=
+ Dimensions of stack rectangle is defined by =rectHeight= and =rectWidth=
+ Starting position of stack coordinates is defined by =rectStartx= and =rectStarty=
+ =topOfStack= Points to top of stack, initialized to -1 and its coordinates
+ Position of arrow pointing to top of stack is define by =arrowStartx= and =arrowEndx=
+ Position of arrow heads to the top of the stack is defined by =arrowHeadOffsetx= and =arrowHeadOffsety=
+ =arrowTopx= and =arrowTopy= are used to define the Coordinates of 'Top' text below arrow
+ Position of error messages is defined by =errorPosx= and =errorPosy=
+ Values of Stack are stored in =values= array

#+NAME: stack_class
#+BEGIN_SRC javascript 
var canvasA = document.getElementById("stack_1");
canvasA.width = 400;
canvasA.height = 340;
var stackA_canvas = canvasA.getContext("2d");
var canvasB = document.getElementById("stack_2");
canvasB.width = 400;
canvasB.height = 340;
var stackB_canvas = canvasB.getContext("2d");
var canvasC = document.getElementById("stack_3");
canvasC.width = 400;
canvasC.height = 340;
var stackC_canvas = canvasC.getContext("2d");
class stack_class {
    constructor(){	    
        this.txtSize = 25;
        this.lineDist = 1;
        this.rectNo = 7;
        this.lineNo = this.rectNo - 1;
        this.rectHeight = 37;
        this.rectWidth = this.rectHeight * 4;	    
        this.rectStartx = 100;
        this.rectStarty = 30;
        this.topOfStack = -1; 
        this.arrowStartx = this.rectStartx + this.rectWidth + 20;
        this.arrowEndx = this.arrowStartx + 40;
        this.arrowHeadOffsetx = 10;
        this.arrowHeadOffsety = 10;
        this.arrowTopx = 5;
        this.arrowTopy = 30;
        this.values = [];
        this.topB = -1;
        this.topC = -1;
        this.demo_values = [];
        this.demo_valuesB = [];
        this.demo_valuesC = [];
        this.interval = 0;
        this.started = 0;
        this.prev = -1;
        this.ispaused = 0;
    };
};
#+END_SRC

* Initialization
#+NAME: initialize
#+BEGIN_SRC javascript
let stack_demo = new stack_class();
#+END_SRC

* Clear the canvas
A function that clears the Canvas and makes it blank.
#+NAME: clear-canvas
#+BEGIN_SRC javascript
function clearCanvas(ctx) {
    if(ctx===stackA_canvas)ctx.clearRect(0, 0, canvasA.width, canvasA.height);
    if(ctx===stackB_canvas)ctx.clearRect(0, 0, canvasB.width, canvasB.height);
    if(ctx===stackC_canvas)ctx.clearRect(0, 0, canvasC.width, canvasC.height);
}
#+END_SRC

* Draw the stack
A basic stack structure is drawn.
#+NAME: draw-array-struct
#+BEGIN_SRC javascript
function drawStackStructure(ctx) {
ctx.beginPath();
ctx.rect(stack_demo.rectStartx, stack_demo.rectStarty, stack_demo.rectWidth, stack_demo.rectHeight*stack_demo.rectNo + stack_demo.lineNo*stack_demo.lineDist);
ctx.strokeStyle = "#969696";
ctx.stroke();

ctx.closePath();
}
#+END_SRC

* Shade elements of stack
As per the designs given by the UI team, all elements of the stack
that are present so far have been shaded with blue color.
#+NAME: shade-top-stack
#+BEGIN_SRC javascript
function writeNumbers(ctx) {
var stackhere = [];
var tophere = -1;
if(ctx===stackA_canvas)
{
   stackhere = stack_demo.demo_values;
   tophere = stack_demo.topOfStack;
}
else if (ctx === stackB_canvas)
{
    stackhere = stack_demo.demo_valuesB.slice();
    tophere = stack_demo.topB;
} 	
else if(ctx === stackC_canvas)
{
    stackhere = stack_demo.demo_valuesC.slice();
    tophere = stack_demo.topC;
}           		
clearCanvas(ctx);
drawStackStructure(ctx);
		
ctx.beginPath();
if(ctx===stackA_canvas)
{
    stackA_canvas.font = "20px";
    stackA_canvas.fillStyle = "black";
    stackA_canvas.fillText("Stack A","150","335");
}
else if(ctx===stackB_canvas)
{
    stackB_canvas.font = "20px";
    stackB_canvas.fillStyle = "black";
    stackB_canvas.fillText("Stack B","150","335");
}
else if(ctx===stackC_canvas)
{
    stackC_canvas.font = "20px";
    stackC_canvas.fillStyle = "black";
    stackC_canvas.fillText("Stack C","150","335");
}
for(var i=0; i<stackhere.length; i++)
{
    ctx.fillStyle = "#288ec8";
    ctx.fillRect(stack_demo.rectStartx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-((i+1)*stack_demo.rectHeight)-(i*stack_demo.lineDist), stack_demo.rectWidth, stack_demo.rectHeight);
}
ctx.closePath();
#+END_SRC

* Write values of stack into array
Function to write all the values present inside the stack into the
stack displayed to user. 
#+NAME: write-values
#+BEGIN_SRC javascript		
ctx.beginPath();
	
for(var i=0; i<stackhere.length; i++) 
{
    ctx.font = "15px Arial";
    ctx.fillStyle = "white";
		    
    txt = stackhere[i].toString();
    txtWidth = ctx.measureText(txt).width;
		    
    txtX = stack_demo.rectStartx + (stack_demo.rectWidth-txtWidth)/2;
    txtY = stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(i*stack_demo.lineDist)-((i+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2+8;
		    
    ctx.fillText(txt, txtX, txtY);  
}

ctx.closePath();
#+END_SRC

* Arrow pointing to the top of the stack 
An arrow points to the top of the stack to indicate where it is. The
code draws a line and then the arrow head.
#+NAME: draw-arrow
#+BEGIN_SRC javascript
if(tophere > -1) 
{
    ctx.beginPath();
		    
    ctx.moveTo(stack_demo.arrowStartx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2);
    ctx.lineTo(stack_demo.arrowEndx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2);
    ctx.strokeStyle = "#a4c652";
    ctx.lineWidth=1.5;
    ctx.stroke();
		    
    ctx.moveTo(stack_demo.arrowStartx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2);
    ctx.lineTo(stack_demo.arrowStartx + stack_demo.arrowHeadOffsetx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2+stack_demo.arrowHeadOffsety);
    ctx.strokeStyle = "#a4c652";
    ctx.stroke();
                		    
    ctx.moveTo(stack_demo.arrowStartx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2);
    ctx.lineTo(stack_demo.arrowStartx + stack_demo.arrowHeadOffsetx, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2-stack_demo.arrowHeadOffsety);
    ctx.strokeStyle = "#a4c652";
    ctx.stroke();

    txt = "Top";
    ctx.font = "18px Arial";
    ctx.fillStyle = "black";
    txtWidth = ctx.measureText(txt).width;
    ctx.fillText(txt, stack_demo.arrowEndx + 10, stack_demo.rectStarty+(stack_demo.lineDist*stack_demo.lineNo)+(stack_demo.rectNo*stack_demo.rectHeight)-(tophere*stack_demo.lineDist)-((tophere+1)*stack_demo.rectHeight)+stack_demo.rectHeight/2+18/2);
    ctx.closePath();
}	
}
#+END_SRC

* Function to check whether a number is prime or not
#+NAME: test-prime
#+BEGIN_SRC javascript
function test_prime(n)
{
  if (n===1)return false;
  else if(n === 2)return true;
  else
  {
    for(var x = 2; x < n; x++)
    {
      if(n % x === 0)
      return false;
      
    }
    return true;  
  }
}
#+END_SRC

* Function Change Interval
+ Supports changing time interval when the slider is changed in the webpage
+ Also, changes the background colour of Pause button if required
#+NAME: change-int
#+BEGIN_SRC javascript
function change_interval()
{
  if(stack_demo.interval != 0) { clearInterval(stack_demo.interval); }
  
  if(document.getElementById("interval").value != 100)
  {
     stack_demo.interval = setInterval(pop_demo, 3400-document.getElementById("interval").value); 
     document.getElementById("pause-stackdemo").style.backgroundColor = "#288ec8";
  }
  else document.getElementById("pause-stackdemo").style.backgroundColor = "grey";
};
#+END_SRC

* Pushing into the stack
Function to push values into the stack and display on screen. 
#+NAME: push-stack
#+BEGIN_SRC javascript
function push_common(ctx,value)
{
    if(ctx === stackB_canvas){
        valueshere = stack_demo.demo_valuesB;
        tophere = stack_demo.topB;
    }
    else if(ctx === stackC_canvas)
    {
        valueshere = stack_demo.demo_valuesC;
        tophere = stack_demo.topC;
    }
    if (value === null || value === "")return;
    if(valueshere.length >= stack_demo.rectNo)return;
    else if (value===null || value==="") return;
    else
    {
	valueshere.push(value);
        tophere++;
        if(ctx===stackB_canvas)
        {
            stack_demo.topB++;
        }
        else if (ctx===stackC_canvas)
        {
            stack_demo.topC++;
        }
    drawStackStructure(stackA_canvas);
    writeNumbers(stackA_canvas);
    drawStackStructure(ctx);
    writeNumbers(ctx);
    }   
}
#+END_SRC

* Popping from the stack
Function to pop values from the stack and display resulting array on screen.
#+NAME: pop-stack
#+BEGIN_SRC javascript
function pop_demo(){
    if(stack_demo.topOfStack === -1)
    {
        document.getElementById("ins").innerHTML = "As stack A is empty.<br><b>Demonstration is complete!!.</b>";
    }
    else
    {
        pop_value = stack_demo.demo_values.pop();
    	stack_demo.topOfStack--;
        if(test_prime(pop_value))
        {
            document.getElementById("ins").innerHTML = "The number popped from stack A (i.e " + pop_value + ") is prime, so it is pushed into stack B";
            push_common(stackB_canvas,pop_value);
        }
        else{
            document.getElementById("ins").innerHTML = "The number popped from stack A (i.e " + pop_value + ") is not a prime, so it is pushed into stack C";
            push_common(stackC_canvas,pop_value);
        }
        if(stack_demo.topOfStack === -1)
        {
            document.getElementById("ins").innerHTML += "<br>As stack A is empty.<br><b>Demonstration is complete!!.</b>";
            document.getElementById("pause-stackdemo").disabled = true;
            document.getElementById("start-stdemo").disabled = true;
            document.getElementById("pause-stackdemo").style.backgroundColor="grey";
            document.getElementById("start-stdemo").style.backgroundColor="grey";
        }
    drawStackStructure(stackA_canvas);
    writeNumbers(stackA_canvas);
    }
}
#+END_SRC

* Function onpause
+ Used to pause the demonstration when =pause= button is clicked 
#+NAME: pause_function
#+BEGIN_SRC javascript
function onpause(){
    if(stack_demo.prev === -1){
    stack_demo.prev = document.getElementById("interval").value;
    if(stack_demo.interval != 0){ 
      clearInterval(stack_demo.interval);
    }
    stack_demo.ispaused = 1;
    document.getElementById("pause-stackdemo").value = "Play";
  }else{
    stack_demo.prev = -1;
    stack_demo.ispaused = 0;
    clearInterval(stack_demo.interval);
    stack_demo.interval = setInterval(pop_demo, 3400-document.getElementById("interval").value);
    document.getElementById("pause-stackdemo").value = "Pause";
  }
}
#+END_SRC

* Function onstart
+ Used to start the demonstration when =start= button is clicked 
+ Used to perform next step when =next= button is clicked
#+NAME: start_function
#+BEGIN_SRC javascript
function onstart(){
document.getElementById("pause-stackdemo").style.backgroundColor="#288ec8";
document.getElementById("reset-stackdemo").style.backgroundColor="#288ec8";
document.getElementById("pause-stackdemo").disabled = false;
document.getElementById("reset-stackdemo").disabled = false;
if(document.getElementById("interval").value === 100 || stack_demo.started ===1)
  {
    document.getElementById("start-stdemo").value = "Next";
    document.getElementById("start-stdemo").disabled = false;
    clearInterval(stack_demo.interval);
    if(stack_demo.ispaused==1){
        pop_demo();
    }
    else{
        pop_demo();
        stack_demo.interval = setInterval(pop_demo, 3400-document.getElementById("interval").value);
    }
    document.getElementById("pause-stackdemo").style.visibility = "visible";
  }
else{
    document.getElementById("ins").innerHTML = "<b>Demonstration started</b>";
    document.getElementById("interval").value === 1500;
    document.getElementById("start-stdemo").value = "Next";
    stack_demo.started = 1;
    change_interval();
  }
}
#+END_SRC

* Function initializeArtefact
+ Function which gets performed on loading the artefact
#+NAME: onload_function 
#+BEGIN_SRC javascript 
function initializeArtefact(){
    stack_demo.topOfStack = 6;
    stack_demo.demo_values = [];
    stack_demo.demo_valuesB = [];
    stack_demo.demo_valuesC = [];
    stack_demo.topB = -1;
    stack_demo.topC = -1;
    while(stack_demo.demo_values.length < 7){
        var random_number = Math.floor((Math.random() * 100) + 1) ;
        if(stack_demo.demo_values.indexOf(random_number) === -1) {
            stack_demo.demo_values.push(random_number);
        }
    }
    document.getElementById("interval").value = 1500;
    document.getElementById("start-stdemo").value = "Start";
    document.getElementById("pause-stackdemo").disabled = true;
    document.getElementById("reset-stackdemo").disabled = true;
    document.getElementById("pause-stackdemo").style.backgroundColor="grey";
    document.getElementById("reset-stackdemo").style.backgroundColor="grey";

    drawStackStructure(stackA_canvas);
    writeNumbers(stackA_canvas);
    drawStackStructure(stackB_canvas);
    writeNumbers(stackB_canvas);
    drawStackStructure(stackC_canvas);
    writeNumbers(stackC_canvas);
    handlers();
}
#+END_SRC

* Function reload
+ Used to reload the artefact when =reset= button is clicked
#+NAME: reload_function
#+BEGIN_SRC javascript
function reload(){
  location.reload(true);
};

#+END_SRC

* Adds all event handlers
+ A function that adds all the =event handlers= to the html document
#+NAME: handlers
#+BEGIN_SRC javascript
function handlers() { 
 document.getElementById("interval").oninput = function() { change_interval(); };
 document.getElementById("interval").onchange = function() { 
    if(document.getElementById("pause-stackdemo").value == "Play")return;
    else change_interval(); };
 document.getElementById("start-stdemo").onclick = function() { onstart(); };
 document.getElementById("reset-stackdemo").onclick = function() { reload(); };
 document.getElementById("pause-stackdemo").onclick = function() { onpause(); };
};
#+END_SRC  

* Tangle
#+BEGIN_SRC javascript :tangle stack-demo.js :eval no :noweb yes
  <<stack_class>>
  <<initialize>>
  <<clear-canvas>>
  <<change-int>>
  <<test-prime>>
  <<draw-array-struct>> 
  <<shade-top-stack>>
  <<write-values>>
  <<draw-arrow>>
  <<pause_function>>
  <<start_function>>
  <<reload_function>>
  <<handlers>>
  <<onload_function>>
  <<push-stack>>
  <<pop-stack>>
#+END_SRC
 
